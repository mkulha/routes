import Ember from 'ember';

export default Ember.Route.extend({
  model: function() {
    return this.store.findAll('gwaypoint');
  },
  actions: {
    setTheMap: function(theMap) {
    Ember.defineProperty(this, 'theMap',
        Ember.computed('options', function() {
          return theMap;
        })
      );
    },
    initializeOptimizedRoutes: function(directionsDisplay, directionsService, waypts) {
      Ember.defineProperty(this, 'directionsDisplay',
        Ember.computed('options', function() {
          return directionsDisplay;
        })
      );
      Ember.defineProperty(this, 'directionsService',
        Ember.computed('options', function() {
          return directionsService;
        })
      );
      Ember.defineProperty(this, 'waypts',
        Ember.computed('options', function() {
          return waypts;
        })
      );
      this.get('directionsDisplay').setMap(this.get('theMap'));
    },
    createGWaypoint: function(newAddress, newLatLng) {
      var geocoder = new window.google.maps.Geocoder();
      var self = this;
      if (Ember.isEmpty(newLatLng)) {
        geocoder.geocode({'address': newAddress}, function(results, status) {
          if (status === window.google.maps.GeocoderStatus.OK) {
            var gwp = self.store.createRecord('gwaypoint', {
              address: newAddress,
              lat:results[0].geometry.location.A,
              lng: results[0].geometry.location.F
            });
            gwp.save();

            self.get('waypts').pushObject({
              location:new window.google.maps.LatLng(gwp.get("lat"), gwp.get("lng")),
              stopover:true
            });

            self.send('calculateRoutes', self, self.modelFor('gmap'));
          } else {
            console.log("Geocode was not successful for the following reason: " + status);
          }
        });
      } else {
        var gwp = self.store.createRecord('gwaypoint', {
          address: newAddress,
          lat: newLatLng.lat(),
          lng: newLatLng.lng()
        });
        gwp.save();

        self.get('waypts').pushObject({
          location:new window.google.maps.LatLng(gwp.get("lat"), gwp.get("lng")),
          stopover:true
        });

        self.send('calculateRoutes', self, self.modelFor('gmap'));
      }
    },
    calculateRoutes: function(self, model) {
      var directionsDisplay = self.get('directionsDisplay');
      var directionsService = self.get('directionsService');

      var start = new window.google.maps.LatLng(model.objectAt(0).get("lat"),
        model.objectAt(0).get("lng"));
      var waypts = self.get('waypts');

      var request = {
        origin:start,
        destination:start,
        travelMode: window.google.maps.TravelMode.DRIVING,
        waypoints: waypts,
        optimizeWaypoints: true,

      };
      directionsService.route(request, function(result, status) {
        if (status === window.google.maps.DirectionsStatus.OK) {
          var myRoute = result.routes[0].waypoint_order;
          var summaryPanel = Ember.$('#map-directions ol')[0];
          summaryPanel.innerHTML = '';

          myRoute.forEach(function(item) {summaryPanel.innerHTML += '<li>' +
            model.objectAt(item).get("address") + '<br>' + model.objectAt(item).get("lat");});

          directionsDisplay.setDirections(result);
        } else {
          console.log("Optimizing the routes did not work for the following reason: " + status);
        }
      });
    }
  }
});