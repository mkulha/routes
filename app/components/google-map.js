import Ember from 'ember';

export default Ember.Component.extend({
  insertMap: function() {
    var model = this.get('model');
    var options = {
        center: new window.google.maps.LatLng(
            model.objectAt(0).get("lat"),
            model.objectAt(0).get("lng")
        )
    };
    Ember.defineProperty(this, 'theMap',
      Ember.computed('options', function() {
        return new window.google.maps.Map(this.$('#map-canvas')[0], options);
      })
    );
    this.sendAction('setTheMap', this.get('theMap'));

    Ember.defineProperty(this, 'directionsDisplay',
        Ember.computed('options', function() {
          return new window.google.maps.DirectionsRenderer();
        })
      );
    Ember.defineProperty(this, 'directionsService',
      Ember.computed('options', function() {
        return new window.google.maps.DirectionsService();
      })
    );
    Ember.defineProperty(this, 'waypts',
      Ember.computed('options', function() {
        return model.map(function(item) {
        return {
            location:new window.google.maps.LatLng(item.get("lat"), item.get("lng")),
            stopover:true
          };
        });
      })
    );

    this.sendAction('initializeOptimizedRoutes', this.get('directionsDisplay'), this.get('directionsService'), this.get('waypts'));

    this.send('calculateRoutes', this, model);

    var self = this;
    window.google.maps.event.addListener(this.get('theMap'), "click", function (event) {
      var gcoder = new window.google.maps.Geocoder();
      gcoder.geocode({'location': event.latLng}, function(results, status) {
        if (status === window.google.maps.GeocoderStatus.OK) {
          if (results[1]) {
            self.send('createGWaypoint', results[1].formatted_address, event.latLng);
          } else {
            console.log('No results found');
          }
        } else {
          console.log("Geocode was not successful for the following reason: " + status);
        }
      });

    });
  }.on('didInsertElement'),
  actions: {
    calculateRoutes: function(self, model) {
      this.sendAction('calculateRoutes', self, model);
    },
    createGWaypoint: function(newAddress, newLatLng) {
      this.sendAction('createGWaypoint', newAddress, newLatLng);
    }
  }
});
