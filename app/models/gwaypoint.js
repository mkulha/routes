import DS from 'ember-data';

export default DS.Model.extend({
  address: DS.attr('string'),
  lat: DS.attr('number'),
  lng: DS.attr('number')
}).reopenClass({
  FIXTURES: [
    {
      id: 1,
      address: 'Slovnaftska 18, Bratislava',
      lat: 48.134720,
      lng: 17.163709
    },
    {
      id: 2,
      address: 'Pristavna, Bratislava',
      lat: 48.139038,
      lng: 17.146469
    },
    {
      id: 3,
      address: 'Mierova, Bratislava',
      lat: 48.1498461,
      lng: 17.1722419
    }
  ]
});
